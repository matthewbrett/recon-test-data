#!/usr/bin/env python

import os
from os import path
import sys

try:
    inst_path = sys.argv[1]
except:
    try:
        import recon
        inst_path = path.split(path.split(recon.__file__)[0])[0]
    except:
        print """
    Could not determine where to install the data.. either install
    Recon Tools or run this script as ./install_testdata install_path.
    """
        sys.exit(1)

sys_cmd = 'tar jxvf rtools_testdata.tar.bz2 -C ' + inst_path
a = os.system(sys_cmd)

print """

    Test data installed.. you can run the tests by typing:
    python -c "import recon; recon.test(level=1)"

    You can also change the level for further tests
"""
